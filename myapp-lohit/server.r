
options(shiny.maxRequestSize=100*1024^2) 

library(shiny)
library(dplyr)
library(MASS)
library(e1071)
library(stringr)
library(entropy)
library(reshape2)
#library(RJDBC)
library(matrixStats)
library(ggplot2)



shinyServer(function(input, output) {
  
  paidinvoices.corpus <- data.frame()
  openinvoices.corpus <- data.frame()
  
  no_of_classes <- 21
  
  # cut range to classes
  breaks <- c(-Inf, seq(5,5*(no_of_classes - 1),5), Inf)
  labels <- paste("C", seq(1,no_of_classes), sep="")
  
  progress <- shiny::Progress$new(min=1, max = 8)
  
  
  ################################# FULL HIST FILE START ####################################

  
  
  
  
  
  
  # get the database connection object
  get_db_connection <- function(username, passwd) {
    driverclass <- "org.netezza.Driver"
    jarpath    <-  "/opt/ibm/nz/lib/nzjdbc3.jar"
    dbName     <-  "jdbc:netezza://pda1-wall.pok.ibm.com:5480//BACC_PRD_ISCNZ_GAPNZ"
    dbNameDev  <-  "jdbc:netezza://dstbld-pda02.bld.dst.ibm.com//BACC_DEV_ISCNZ_GAPNZ"  
    dbNameJosh <- "jdbc:netezza://pda02.bld.dst.ibm.com:5480//BACC_DEV_CSP_CDO"
    
    drv <- JDBC(driverclass, jarpath, identifier.quote="`")
    conn <- dbConnect(drv, dbName, username, passwd)
    return(conn)
  }
  
  
  # fetch the paid invoice table from the db
  get_all_invoices <- function(conn) {
    query <- 'select CUSTNO, INVNO, TIMESTAMP, COMPANY, ENTERPRISE, BILLTYPE, 
    INVDATE, AGEDATE, CLEARDATE, INVSTATUS from ARPAYTREND.INVPAYDET'
    
    
    res <- dbGetQuery(conn, query)
    res.df <- as.data.frame(res)
    names(res.df) <- dimnames(res)[[2]]
    gc()
    return(res.df)
    
  }
  
  # fetch the paidact table from the db
  get_action_data <- function(conn) {
    query <- 'select CUSTNO, INVNO, TIMESTAMP, ACTIONDATE, ACTION from
    US_LEA_STAGING.PAIDACT' 
    res <- dbGetQuery(conn, query)
    res.df <- as.data.frame(res)
    names(res.df) <- dimnames(res)[[2]]
    gc()
    return(res.df)
  }
  
  get_payment_amt <- function(conn) {
    query <- 'select INVPAY  from
    US_LEA_STAGING.PAID' 
    res <- dbGetQuery(conn, query)
    res.df <- as.data.frame(res)
    names(res.df) <- dimnames(res)[[2]]
    gc()
    return(res.df)
  }
  
  # refresh open actions and open invoices in local cache
  refresh_cache_open <- function() {
    conn <- get_db_connection("rnahar", "Sfeb2016") 
    res.inv <-get_all_invoices(conn)
    res.act <- get_action_data_open(conn)
    
    res.inv <- filter(res.inv, INVSTATUS == "open")
    res.inv$TIMESTAMP <- paste("TS", as.character(res.inv$TIMESTAMP), sep="")
    res.act$TIMESTAMP <- paste("TS", as.character(res.act$TIMESTAMP), sep="")
    
    write.csv(res.inv, paste(cacheDir, '/OpenInvoices.csv', sep=""), row.names=F)
    write.csv(res.act, paste(cacheDir, '/OpenActions.csv', sep=""), row.names=F)
    
  }
  
  # refresh paid actions and paid invoices in local cache
  refresh_cache_paid <- function() {
    conn <- get_db_connection("rnahar", "Sfeb2016") 
    res.inv <-get_all_invoices(conn)
    res.act <- get_action_data(conn)
    
    res.inv <- filter(res.inv, INVSTATUS == "paid")
    res.inv$TIMESTAMP <- paste("TS", as.character(res.inv$TIMESTAMP), sep="")
    res.act$TIMESTAMP <- paste("TS", as.character(res.act$TIMESTAMP), sep="")
    
    write.csv(res.inv, paste(cacheDir, '/PaidInvoices.csv', sep=""), row.names=F)
    write.csv(res.act, paste(cacheDir, '/PaidActions.csv', sep=""), row.names=F)
    
  }
  
  
  action_code_to_act_type <- function(x) {
    result <- "OTHER"
    
    if (x == "ECUS") {
      result <- "EMAIL"
    }  
    
    if (x == "TCUS") {
      result <- "CALL"
    } 
    
    if (x %in% c("CDIS", "CRMT")) {
      result <- "DISPUTED"
    } 
    
    if (x == "DRES") {
      result <- "DISPUTE_RESOLVE"
    } 
    
    if (x %in% c("MKTG", "ECOM", "MGTE")) {
      result <- "ESCALATION"
    } 
    
    return(result)
  }
  
  # Utility functions for date manipulation
  # ------------------------------------------------------------------------------
  get_days_diff <- function(dt1, dt2, fmt) {
    dt1.posix <- as.POSIXlt(strptime(dt1, format = fmt))
    dt2.posix <- as.POSIXlt(strptime(dt2, format = fmt))
    return (as.integer(difftime(dt1.posix, dt2.posix, units="days")))
  }
  
  get_ref_date <- function() {
    return("2014-01-01")
  }
  
  ## convert a date to a day index
  get_day_index <- function(date) {
    return(get_days_diff(date, get_ref_date(), fmt="%Y-%m-%d"))
  }
  
  get_mday <- function(date){ 
    dt1.posix <- (as.POSIXlt(strptime(date, format = "%Y-%m-%d"))) 
    return (dt1.posix$mday)
  }
  
  get_month <- function(date){
    dt.posix <- (as.POSIXlt(strptime(date, format = "%Y-%m-%d"))) 
    return(dt.posix$mon + 1)
  }
  
  get_month_diff <- function(start_date, end_date){
    s_date <- (as.POSIXlt(strptime(start_date, format = "%Y-%m-%d")))
    e_date <- (as.POSIXlt(strptime(end_date, format = "%Y-%m-%d")))
    year_diff <- (e_date$year - s_date$year)
    month_diff <- (year_diff*12 + (e_date$mon-s_date$mon))
    return (month_diff)
  }
  
  #Returns the last date of the next month
  get_month_end <- function(month, year){
    if(month == 2 & year %% 4 == 0 ){
      return(29)
    }
    else if(month == 2 & year %% 4 != 0){
      return(28)
    }
    
    month_days <- c(31,28,31,30,31,30,31,31,30,31,30,31)
    return (month_days[month])
    
  }
  
  
  get_month_bin <- function(inv_date, clear_date){
    i_date <- (as.POSIXlt(strptime(inv_date, format = "%Y-%m-%d")))
    c_date <- (as.POSIXlt(strptime(clear_date, format = "%Y-%m-%d")))
  }
  
  
  get_month_from_date <- function (dt, fmt = "%Y-%m-%d") 
  {
    dt.posix <- as.POSIXlt(strptime(dt, format = fmt))
    return(dt.posix$mon)
  }
  
  get_year_from_date <- function (dt, fmt = "%Y-%m-%d") 
  {
    dt.posix <- as.POSIXlt(strptime(dt, format = fmt))
    return(dt.posix$year)
    
  }
  
  get_month_day_from_date <- function (dt, fmt = "%Y-%m-%d") 
  {
    dt.posix <- as.POSIXlt(strptime(dt, format = fmt))
    return(dt.posix$mday)
  }
  
  
  # -----------------------------------------------------------------------------
  
  
  # initialize the corpus 
  initialize_corpus_from_cache <- function(paidInv) {
    # Read in the cache and append the days from refdate fields
 #   paidInvFile <- paste(cacheDir, '/PaidInvoicesJune16.csv', sep="")
#    openInvFile <- paste(cacheDir, '/OpenInvoicesJune16.csv', sep="")
    
  #  paidinvoices.corpus <- read.csv(paidInvFile, header=T, stringsAsFactors=T, na.strings=c("", " ", "NA"))
  #  openinvoices.corpus <- read.csv(openInvFile, header=T, stringsAsFactors=T, na.strings=c("", " ", "NA"))
   
    paidinvoices.corpus <<- paidInv
    #openinvoices.corpus <- OpenInv
   
    
    paidinvoices.corpus$TIMESTAMP <<- as.character(paidinvoices.corpus$TIMESTAMP)
    paidinvoices.corpus$INV_N_DAY <<- get_day_index(paidinvoices.corpus$INVDATE)
    paidinvoices.corpus$CLEAR_N_DAY <<- get_day_index(paidinvoices.corpus$CLEARDATE)
    paidinvoices.corpus$AGE_N_DAY <<- get_day_index(paidinvoices.corpus$AGEDATE)
    
    paidinvoices.corpus <<- mutate(paidinvoices.corpus, 
                                   DAYS_TO_PAY = CLEAR_N_DAY - INV_N_DAY, 
                                   PAYWINDOW = AGE_N_DAY - INV_N_DAY)
    
    paidinvoices.corpus <<- filter(paidinvoices.corpus, CLEAR_N_DAY >= 350)
    
    #For Open Invoices
    #openinvoices.corpus$TIMESTAMP <- as.character(openinvoices.corpus$TIMESTAMP)
    #openinvoices.corpus$INV_N_DAY <- get_day_index(openinvoices.corpus$INVDATE)
    #openinvoices.corpus$CLEAR_N_DAY <- get_day_index(openinvoices.corpus$CLEARDATE)
    #openinvoices.corpus$AGE_N_DAY <- get_day_index(openinvoices.corpus$AGEDATE)
    
    #openinvoices.corpus <- mutate(openinvoices.corpus, 
    #                               PAYWINDOW = AGE_N_DAY - INV_N_DAY)
    
  #  openinvoices.corpus <- filter(openinvoices.corpus, CLEAR_N_DAY >= 350)
    
    #paidActionFile <- paste(cacheDir, '/PaidActions.csv', sep="")
    #paidactions.dat <- read.csv(paidActionFile, header=T, stringsAsFactors=T, na.strings=c("", " ", "NA"))
    
    #ACT_MAT <- as.matrix(paidactions.dat$ACTION, ncol=1)
    #paidactions.dat$ACT_TYPE <- apply(ACT_MAT, 1, action_code_to_act_type)
    #paidactions.dat$ACTION_N_DAY <- get_day_index(paidactions.dat$ACTIONDATE)
    #paidactions.corpus <- paidactions.dat
    
    #paidactions.dat <- paidactions.dat %>% group_by(INVNO, CUSTNO, TIMESTAMP) %>% 
    #  summarise(
    #    INV_NUM_EMAIL = sum(ACT_TYPE == "EMAIL"),
    #    INV_NUM_CALL  = sum(ACT_TYPE == "CALL"),
    #    INV_NUM_ESCALATION = sum(ACT_TYPE == "ESCALATION"),
    #    INV_NUM_DISPUTED  = sum(ACT_TYPE == "DISPUTED")) %>% ungroup()
    
    #paidinvoices.corpus <- left_join(paidinvoices.corpus, paidactions.dat, by=c("INVNO", "CUSTNO", "TIMESTAMP"))
    #Only take positive amount value
    #paidinvoices.corpus <- filter(paidinvoices.corpus, GROSSINV > 0)
    #dupl <- duplicated(paidinvoices.corpus[,c("CUSTNO","INVNO","TIMESTAMP")])
    #paidinvoices.corpus <- paidinvoices.corpus[!dupl,]
    paidinvoices.corpus[is.na(paidinvoices.corpus)] <<- 0
   # openinvoices.corpus[is.na(openinvoices.corpus)] <- 0
    
    
    
  }
  
  
  # get a handle to paid invoices corpus
  get_corpus_paid_invoices <- function() {
    return(paidinvoices.corpus)
  }
  
  get_corpus_paid_actions <- function() {
    return(paidactions.corpus)
  }
  get_corpus_open_invoices <- function() {
    return(openinvoices.corpus)
  }
  
  get_corpus_open_actions <- function() {
    return(openactions.corpus)
  }
  
  
  
  # A mini missing value library to deal with missing values
  
  # ------------------------------------------------------------------
  
  
  # impute missing values with median
  impute_value <- function(x) {
    x[is.na(x)] <- quantile(x, probs=0.5, na.rm=T)
    return(x)
  }
  
  # replace missing value with  a constant.
  replace_missing <- function(x,c) {
    x[is.na(x)] <- c
    return(x)
  }
  
  # copy missing values from another array
  copy_value  <- function(x,y) {
    ind <- which(is.na(x))
    x[ind] <- y[ind]
    return(x)
  }
  
  # -------------------------------------------------------------------
  
  get_day_index <- function(date) {
    return(get_days_diff(date, get_ref_date(), fmt="%Y-%m-%d"))
  }
  
  
  # generate a snapshot of invoices open on a day
  generate_snapshot <- function(date) {
    print(paste("Creating Snapshot:", date))
    
    mday <- get_day_index(date)
    paid.inv <- paidinvoices.corpus %>% filter(INV_N_DAY <= mday & CLEAR_N_DAY > mday & INV_N_DAY >= mday - 120)
    #open.inv <- get_corpus_open_invoices() %>% filter(INV_N_DAY <= mday & INV_N_DAY >= mday - 120)
    #open.names <- names(open.inv)
    #paid.inv <- dplyr::select_(paid.inv, open.names,.dots=open.names)
    #all.inv <- rbind(paid.inv, open.inv)
    
    all.inv <- paid.inv
    #Get the open invoices also
    
    all.inv <- mutate(all.inv, DAYS_OPEN = mday - INV_N_DAY)
    
    #all.act <- get_corpus_paid_actions() %>% filter(ACTION_N_DAY <= mday)
    
    #all.act <- all.act %>% group_by(CUSTNO, INVNO, TIMESTAMP) %>%
    #  summarise(INV_NUM_EMAIL = sum(ACT_TYPE == "EMAIL"),
    #            INV_NUM_CALL  = sum(ACT_TYPE == "CALL"),
    #            INV_NUM_ESC   = sum(ACT_TYPE == "ESCALATION"),
    #            INV_NUM_DISPUTED = sum(ACT_TYPE == "DISPUTED")) %>% ungroup()
    
    
    #all.act <- left_join(open.inv, all.act, by=c("CUSTNO", "INVNO", "TIMESTAMP"))
    #all.act[is.na(all.act)] <- 0
    
    
    return (all.inv)
  }
  
  
  
  class_allocation <- function(num) {
    if(num <=0){
      return ("C1")
    }
    else if (num > 5*(no_of_classes-1)){
      return (paste("C",no_of_classes, sep=""))
    }
    else{
      c=floor((num-1)/5) + 1 
      return (paste("C",c,sep=""))
    }
  }
  
  # Takes the training data with COMPANY, ENTERPRISE, BILLTYPE, true_class
  # and builds a dataframe with COMPANY, ENTERPRISE, BILLTYPE, C1, ....., Ck (histogram), entropy
  get_histogram <- function(dat) {
    
    dat <- dplyr::select(dat, COMPANY, ENTERPRISE, BILLTYPE, true_class)
    
    countTable <- dat %>% group_by(COMPANY, ENTERPRISE, BILLTYPE, true_class) %>% summarise(COUNT=n()) %>%
      ungroup()
    
    countTableWide <- dcast(countTable, COMPANY+ENTERPRISE+BILLTYPE ~ true_class, value.var = "COUNT")
    countTableWide[is.na(countTableWide)] <- 0
    countTableWide$entropy <- apply(countTableWide[, 4:ncol(countTableWide)], 1, entropy)
    countTableWide$HISTCOUNT <- apply(countTableWide[, 4:ncol(countTableWide)], 1, sum)
    countTableWide$entropy[is.na(countTableWide$entropy)] <- 3.0
    return(countTableWide)
  }
  
  generate_month_aligned_histogram <- function(dat) {
    dat1 <- distinct(dplyr::select(dat, COMPANY,ENTERPRISE,BILLTYPE, IS_DOMINANT))
    
    dat <- dplyr::select(dat, COMPANY, ENTERPRISE, BILLTYPE, clear_day_label, shift_index)
    
    countTable <- dat %>% group_by(COMPANY, ENTERPRISE, BILLTYPE, clear_day_label, shift_index ) %>% summarise(COUNT=n()) %>%
      ungroup()
    
    countTableWide <- dcast(countTable, COMPANY+ENTERPRISE+BILLTYPE ~ clear_day_label+shift_index, value.var = "COUNT")
    countTableWide[is.na(countTableWide)] <- 0
    countTableWide$MONTH_ALIGNED_ENTROPY <- apply(countTableWide[, 4:ncol(countTableWide)], 1, entropy)
    
    countTableWide$histCounts <- apply(countTableWide[ , grep("P._.", names(countTableWide))], 1, sum)
    
    countTableWide$MONTH_ALIGNED_ENTROPY[is.na(countTableWide$MONTH_ALIGNED_ENTROPY)] <- 4.0
    
    countTableWide <- left_join(countTableWide,dat1, by=c("COMPANY","ENTERPRISE", "BILLTYPE"))
    #dat1 <- distinct(dplyr::select(dat, COMPANY,ENTERPRISE,BILLTYPE, IS_DOMINANT))
    
    
    return(countTableWide)
  }
  
  
  generate_full_histogram <- function(dat) {
    result <- get_histogram(dat)
    names(result)[4:ncol(result)] <- paste("FULL_", names(result)[4:ncol(result)], sep="")
    return (result)
  }
  
  generate_recent_histogram <- function(dat, runDate) {
    mday <- get_day_index(runDate)
    dat.recent <- filter(dat, CLEAR_N_DAY >= mday - 90 & CLEAR_N_DAY <= mday)
    hist.recent <- get_histogram(dat.recent)
    names(hist.recent)[4:ncol(hist.recent)] <- paste("RECENT_", names(hist.recent)[4:ncol(hist.recent)], sep="")
    return(hist.recent)
  }
  
  generate_custno_histogram <- function(dat){
    
    dat <- dplyr::select(dat, CUSTNO, true_class)
    group_cnt <- dat %>% group_by(CUSTNO) %>% summarise(g_cnt = n()) %>% ungroup()
    dat <- left_join(dat, group_cnt, by=c("CUSTNO"))
    dat <- filter(dat, g_cnt > 100);
    
    countTable <- dat %>% group_by(CUSTNO, true_class) %>% summarise(COUNT=n()) %>%
      ungroup()
    
    countTableWide <- dcast(countTable, CUSTNO ~ true_class, value.var = "COUNT")
    countTableWide[is.na(countTableWide)] <- 0
    names(countTableWide)[2:ncol(countTableWide)] <- paste("CUSTNO_", names(countTableWide)[2:ncol(countTableWide)], sep="")
    countTableWide$CUSTNO_entropy <- apply(countTableWide[, 2:ncol(countTableWide)], 1, entropy)
    return(countTableWide)
    
  }  
  
  get_indices <- function(predClass) {
    
    return(as.numeric(strsplit(predClass, "_")[[1]][2]))
  }
  
  get_classes <- function(predClass) {
    
    foo <- data.frame(P1 = 1, P2 = 2, P3 = 3)
    key <- strsplit(predClass, "_")[[1]][1]
    
    return (as.numeric(foo[key]))
    
  }
  
  
  predict_days_to_pay_ma <- function(days_open_class, inv_day_class, invdate, 
                                     shift_index, ma_entropy, ma_hist) {
    
    shift_index <- as.numeric(shift_index)
    ma_entropy  <- as.numeric(ma_entropy)
    #ma_hist     <- as.numeric(ma_hist)
    
    
    # Stub Run
    #days_open_class <- test.fullhist$days_open_label[1]
    #inv_day_class   <- test.fullhist$inv_day_label[1]
    #invdate         <- test.fullhist$INVDATE[1]
    #shift_index     <- test.fullhist$shift_index[1]
    #ma_entropy      <- test.fullhist$MONTH_ALIGNED_ENTROPY[1]
    #ma_hist         <- test.fullhist[1, grep("P._.", names(test.fullhist))]
    
    inv_month <- get_month_from_date(invdate)
    inv_year  <- get_year_from_date(invdate)
    inv_day   <- get_month_day_from_date(invdate)
    
    
    ma_indices <- sapply(names(ma_hist), get_indices)
    ma_classes <- sapply(names(ma_hist), get_classes)
    
    day_class <- get_classes(as.character(days_open_class))
    inv_class <- get_classes(as.character(inv_day_class))
    
    
    exclude_indices_1 <- which(ma_indices < shift_index)
    
    class_names <- c("P1", "P2", "P3")
    x <- 1 + ( c(inv_class-1, inv_class, inv_class+1) %% 3)
    xi <- which(x == day_class)
    xbad <- x[1:(xi-1)]
    
    exclude_indices_2 <- which((ma_indices == shift_index) & (ma_classes %in% xbad))
    
    ma_hist_good <- ma_hist[-c(exclude_indices_1, exclude_indices_2)]
    ma_indices_good <- ma_indices[-c(exclude_indices_1, exclude_indices_2)]
    ma_classes_good <- ma_classes[-c(exclude_indices_1, exclude_indices_2)]
    
    predicted_class <- which.max(as.numeric(ma_hist_good))
    pred_index <- ma_indices_good[predicted_class]
    pred_class <- ma_classes_good[predicted_class]
    pred_dates <- c(10,20,28)
    
    
    pred_day <- pred_dates[pred_class]
    
    adjust <- 0
    
    #print(paste("inv_class, pred_class, predicted_class", inv_class, pred_class, predicted_class))
    
    if ((inv_class == 3 & pred_class == 1) |
          (inv_class == 3 & pred_class == 2) |
          (inv_class == 2 & pred_class == 1)) {
      adjust <- 1
    }
    
    if (inv_class != 3) {
      pred_month <- inv_month + pred_index -1 + adjust
      pred_year <- inv_year + as.integer(pred_month / 12)
      pred_month <- pred_month %% 12
      pred_year <- 1900+pred_year # R- specific conversion
      
      pred_date <- paste(pred_year, pred_month+1, pred_day, sep="-")
      return(get_days_diff(pred_date, invdate, fmt="%Y-%m-%d")) 
    }
    
    if (inv_day <= 5) {
      new_inv_date <- as.Date(invdate) - 5
      inv_month <- get_month_from_date(new_inv_date)
      inv_year  <- get_year_from_date(new_inv_date)
      inv_day   <- get_month_day_from_date(new_inv_date)
      
    }
    
    pred_month <- inv_month + pred_index - 1 + adjust
    pred_year <- inv_year + as.integer(pred_month / 12)
    pred_month <- pred_month %% 12 # R - specific
    pred_year <- 1900+pred_year # R- specific conversion
    
    pred_date <- paste(pred_year, pred_month+1, pred_day, sep="-")
    return(get_days_diff(pred_date, invdate, fmt='%Y-%m-%d'))
    
  } 
  
  
  predict_days_to_pay <- function(days_open, entropy, histogram) {
    days_open_class <- cut(days_open, breaks=breaks, labels=labels)
    
    rem_histogram <- histogram[as.numeric(days_open_class):length(histogram)]
    #rem_entropy <- entropy(rem_histogram)
    if (entropy < 0.5) {
      max_class <- which.max(rem_histogram)
      diff_class <- as.numeric(max_class) - 1
      return (days_open + 5 * diff_class)
    }
    
    if (as.numeric(days_open_class) < 21) {
      if(sum(rem_histogram) == 0){
        rem_histogram <- rem_histogram + 1
      }
      values <- breaks[(1+ as.numeric(days_open_class)): (length(breaks) - 1)] - 2.5
      values <- c(values, 120)
      return (weightedMedian(values, rem_histogram))
    }
    
    return (120)
    
    
    
  }
  
  
  predict_splitClassifier_all <- function(ma_hist, full_hist, test.data){
    
    # Stub Run:  
    #  ma_hist <- sample.ma.hist
    #  test.data <- test1.data[1:100, ]
    #  ma_hist <- full_histogram  
    
    orig_cols <- names(test.data)
    
    test.fullhist <- left_join(test.data, ma_hist, by=c("COMPANY", "ENTERPRISE", "BILLTYPE"))
    test.fullhist <- left_join(test.fullhist, full_hist, by = c("COMPANY", "ENTERPRISE", "BILLTYPE"))
    idxComplete <- complete.cases(test.fullhist)
    
    #test.fullhist.notcomplete <- test.fullhist[-idxComplete, ]
    test.fullhist <- test.fullhist[idxComplete, ]
    
    
    print(paste("Complete Cases =", nrow(test.fullhist)))
    
    write.csv(test.fullhist, 'C_Testfile.csv', row.names=F)
    
    
  }
  
  
  
  predict_splitClassifier_ma <- function(ma_hist, test.data){
    
    # Stub Run:  
    #  ma_hist <- sample.ma.hist
    #  test.data <- test1.data[1:100, ]
    #  ma_hist <- full_histogram  
    
    orig_cols <- names(test.data)
    
    test.fullhist <- left_join(test.data, ma_hist, by=c("COMPANY", "ENTERPRISE", "BILLTYPE"))
    idxComplete <- complete.cases(test.fullhist)
    
    #test.fullhist.notcomplete <- test.fullhist[-idxComplete, ]
    test.fullhist <- test.fullhist[idxComplete, ]
    
    print(paste("Complete Cases =", nrow(test.fullhist)))
    
    
    daysOpenClassCol <- grep("days_open_label", names(test.fullhist))
    invDayClassCol   <- grep("inv_day_label", names(test.fullhist))
    invDateCol       <- grep("INVDATE", names(test.fullhist))
    shiftIndexCol    <- grep("shift_index", names(test.fullhist))
    ma_Cols          <- grep("P._.", names(test.fullhist))
    entropyCol       <- grep("MONTH_ALIGNED_ENTROPY", names(test.fullhist))
    
    
    
    #idxComplete <- which(complete.cases(test.fullhist))
    
    #test.fullhist <- test.fullhist[idxComplete, ]
    test.fullhist$pred_dtp <- apply(test.fullhist[, c(daysOpenClassCol, invDayClassCol, invDateCol,
                                                      shiftIndexCol, entropyCol, ma_Cols)], 1,
                                    function(x) { predict_days_to_pay_ma(x[1], x[2], x[3], x[4], x[5], x[6:length(x)]) })
    
    
    test.fullhist <- dplyr::select_(test.fullhist, .dots=c(orig_cols, "pred_dtp"))
    result_table <- test.fullhist
    
    return (result_table)
  }
  get_rem_entropy <- function(days_open, histogram){
    days_open_class <- cut(days_open, breaks=breaks, labels=labels)
    if(days_open_class == 21){
      return(-1)
    }
    rem_histogram <- histogram[as.numeric(days_open_class):length(histogram)]
    
    rem_entropy <- entropy(rem_histogram)
    return (rem_entropy)
  }
  predict_splitClassifier <- function(custno_hist, recent_hist, full_hist, test.data){
    orig_cols <- names(test.data)
    
    test.fullhist <- left_join(test.data, full_hist, by=c("COMPANY", "ENTERPRISE", "BILLTYPE"))
    idxComplete <- complete.cases(test.fullhist)
    
    #test.fullhist.notcomplete <- test.fullhist[-idxComplete, ]
    test.fullhist1 <- test.fullhist[idxComplete, ]
    
    
    test.rem <- test.fullhist[!idxComplete,]
    if(nrow(test.rem) > 0){
      test.rem <- test.rem[,grep("FULL_", names(test.rem), invert=T)]
      test.rem <- test.rem %>% mutate(join_attr = 1)
      full.hist <- dplyr::select(full_hist, grep("FULL_",names(full_hist)))
      global_hist <- apply(full.hist,2,sum)
      global_hist <- data.frame(t(global_hist))
      global_hist$FULL_entropy <- entropy(as.numeric(dplyr::select(global_hist, grep("FULL_C", names(global_hist)))))
      global_hist <- global_hist %>% mutate(join_attr = 1)
      
      test.fullhist2 <- left_join(test.rem, global_hist, by=c("join_attr"))
      test.fullhist2$join_attr <- NULL
      test.fullhist <- rbind(test.fullhist1, test.fullhist2)
    }
    else{
      test.fullhist <- test.fullhist1
    }
    daysOpenCol <- grep("DAYS_OPEN", names(test.fullhist))
    fullCols  <- grep("FULL_C", names(test.fullhist))
    entropyCol <- grep("FULL_entropy", names(test.fullhist))
    
    
    #idxComplete <- which(complete.cases(test.fullhist))
    
    #test.fullhist <- test.fullhist[idxComplete, ]
    test.fullhist$pred_dtp <- apply(test.fullhist[, c(daysOpenCol, entropyCol, fullCols)], 1,
                                    function(x) { predict_days_to_pay(x[1], x[2], x[3:length(x)]) })
    #test.fullhist$rem_entropy <- apply(test.fullhist[, c(daysOpenCol,  fullCols)], 1,
    #                                   function(x) { get_rem_entropy(x[1], x[2:length(x)]) })
    
    test.fullhist <- dplyr::select_(test.fullhist, .dots=c(orig_cols, "pred_dtp", "FULL_entropy"))
    result_table <- test.fullhist
    
    return (result_table)
  }
  
  
  
  generate_training_data <- function(date) {
    mday <- get_day_index(date)
    train.data <- paidinvoices.corpus %>% filter(CLEAR_N_DAY < mday)
    
    
    train.data <- append_extra_cols_train(train.data)
    #train.data <- append_extra_month_aligned_cols(train.data)
    
    cat("Stripping training data after date =", date)
    train.data <- filter(train.data, CLEAR_N_DAY <= mday)
    
    return(train.data)
  }
  
  #@ Appends group_cnt and true_class to training records to enable building the 
  # histograms later
  append_extra_cols_train <- function(train.data){
    names(train.data)
    group_cnt <- train.data %>% group_by(COMPANY,ENTERPRISE,BILLTYPE) %>% summarise(group_cnt=n()) %>% ungroup()
    train.data <- left_join(train.data, group_cnt, by=c("COMPANY","ENTERPRISE","BILLTYPE"))
    
    #Eliminate all the combinations with fewer training examples
    #train.data <- filter(train.data, group_cnt > 50)
    
    train.data$INV_N_DAY <- get_day_index(train.data$INVDATE)
    train.data$CLEAR_N_DAY <- get_day_index(train.data$CLEARDATE)
    train.data$AGE_N_DAY <- get_day_index(train.data$AGEDATE)
    
    train.data <- mutate(train.data, DAYS_TO_PAY=CLEAR_N_DAY - INV_N_DAY)
    train.data <- mutate(train.data, PAY_WINDOW=AGE_N_DAY - INV_N_DAY)
    
    
    train.data$true_class <- cut(train.data$DAYS_TO_PAY, breaks=breaks, labels=labels)
    
    #dtp <- data.frame(train.data$DAYS_TO_PAY)
    #true_class <- apply(dtp, 1, class_allocation)
    #train.data <- cbind(train.data, true_class)
    
    #train.data$true_class <- factor(train.data$true_class)
    
    return(train.data)
  }
  
  
  #@ take a dataframe consisting of INVDATE and CLEARDATE
  #@ and augment with a column denoting shift index
  compute_shift_index <- function(x) {
    inv_ref_year <- x[1]
    inv_ref_month <- x[2]
    inv_refday   <- x[3]
    clear_ref_year <- x[4]
    clear_ref_month <- x[5]
    clear_ref_day  <- x[6]
    
    
    shift_index <- (clear_ref_year - inv_ref_year) * 12 + (clear_ref_month - inv_ref_month) + 1
    return(shift_index)
  }
  
  #@ assign reference date for a cycle
  compute_ref_days <- function(x) {
    month_day <- get_month_day_from_date(x)
    if (month_day <= 5) {
      new_date <- as.Date(x) - 6
    } else {
      new_date <- as.Date(x)
    }
    
    return(as.character(new_date))
  }
  
  
  adjust_shift_index <- function(x) {
    inv_day_label <- x[1]
    clear_day_label <- x[2]
    orig_shift <- as.numeric(x[3])
    #clear_day  <- as.numeric(x[4])
    #inv_day    <- as.numeric(x[5])
    
    
    #print(paste("orig_shift = ", class(orig_shift)))
    
    adjust <- 0
    
    if (inv_day_label == "P1" & clear_day_label == "P1") 
      adjust <- 0
    
    if (inv_day_label == "P2" & clear_day_label == "P1")
      adjust <- -1
    
    if (inv_day_label == "P3" & clear_day_label == "P1")
      adjust <- -1
    
    if (inv_day_label == "P3" & clear_day_label == "P2")
      adjust <- -1
    
    return (orig_shift+adjust)
    
  }  
  
  # append exta columns to test vector
  append_extra_cols_pre_test <- function(test.data, runDate) {
    # append extra columns corresponding to company, billtype month aligned payments
    test.data$RUNDATE <- runDate
    test.data$run_day <- get_month_day_from_date(test.data$RUNDATE)
    sample.dat <- test.data
    
    breaks_m = c(-Inf,4,15,25,Inf)
    labels_m = c("END_UPPER", "P1", "P2", "END_LOWER")
    
    sample.dat$inv_day   <- get_month_day_from_date(sample.dat$INVDATE)
    sample.dat$inv_year   <- get_year_from_date(sample.dat$INVDATE)
    sample.dat$inv_month   <- get_month_from_date(sample.dat$INVDATE)
    
    
    inv_day_lt_5 <- which(sample.dat$inv_day < 5)
    run_day_lt_5 <- which(sample.dat$run_day < 5)
    
    sample.dat$REF_INV_DATE <- as.character(sample.dat$INVDATE)
    sample.dat$REF_INV_DATE[inv_day_lt_5] <- as.character(as.Date(sample.dat$INVDATE[inv_day_lt_5]) - 4)
    
    sample.dat$REF_RUN_DATE <- as.character(sample.dat$RUNDATE)
    sample.dat$REF_RUN_DATE[run_day_lt_5] <- as.character(as.Date(sample.dat$CLEARDATE[run_day_lt_5]) - 4)
    
    sample.dat$run_ref_day <- get_month_day_from_date(sample.dat$REF_RUN_DATE)
    sample.dat$inv_ref_day   <- get_month_day_from_date(sample.dat$REF_INV_DATE)
    sample.dat$run_ref_year <- get_year_from_date(sample.dat$REF_RUN_DATE)
    sample.dat$inv_ref_year   <- get_year_from_date(sample.dat$REF_INV_DATE)
    sample.dat$run_ref_month <- get_month_from_date(sample.dat$REF_RUN_DATE)
    sample.dat$inv_ref_month   <- get_month_from_date(sample.dat$REF_INV_DATE)
    
    
    
    sample.dat$days_open_label <- cut(sample.dat$run_ref_day, breaks=breaks_m, labels=labels_m)
    sample.dat$inv_day_label <- cut(sample.dat$inv_day, breaks=breaks_m, labels=labels_m)
    levels(sample.dat$days_open_label) <- c("P3", "P1", "P2","P3")
    levels(sample.dat$inv_day_label) <- c("P3", "P1", "P2","P3")
    
    
    sample.dat.subset <- dplyr::select(sample.dat, inv_ref_year, inv_ref_month, inv_ref_day, run_ref_year, run_ref_month, run_ref_day)
    # Is it the first encountered month bin ? the second or third etc.
    sample.dat$shift_index <- apply(sample.dat.subset, 1, compute_shift_index)
    sample.dat.subset <- dplyr::select(sample.dat, inv_day_label, days_open_label, shift_index)
    sample.dat$shift_index <- apply(sample.dat.subset, 1, adjust_shift_index)
    
    
    return(sample.dat)
  }
  
  # append extra columns corresponding to company, billtype month aligned payments
  append_extra_month_aligned_cols <- function(train.data) {
    sample.dat <- train.data
    
    breaks_m = c(-Inf,4,15,25,Inf)
    labels_m = c("END_UPPER", "P1", "P2", "END_LOWER")
    
    sample.dat$clear_day <- get_month_day_from_date(sample.dat$CLEARDATE)
    sample.dat$inv_day   <- get_month_day_from_date(sample.dat$INVDATE)
    sample.dat$clear_year <- get_year_from_date(sample.dat$CLEARDATE)
    sample.dat$inv_year   <- get_year_from_date(sample.dat$INVDATE)
    sample.dat$clear_month <- get_month_from_date(sample.dat$CLEARDATE)
    sample.dat$inv_month   <- get_month_from_date(sample.dat$INVDATE)
    
    sample.dat$clear_day_label <- cut(sample.dat$clear_day, breaks=breaks_m, labels=labels_m)
    sample.dat$inv_day_label <- cut(sample.dat$inv_day, breaks=breaks_m, labels=labels_m)
    levels(sample.dat$clear_day_label) <- c("P3", "P1", "P2","P3")
    levels(sample.dat$inv_day_label) <- c("P3", "P1", "P2","P3")
    
    inv_day_lt_5 <- which(sample.dat$inv_day < 5)
    clear_day_lt_5 <- which(sample.dat$clear_day < 5)
    
    sample.dat$REF_INV_DATE <- as.character(sample.dat$INVDATE)
    sample.dat$REF_INV_DATE[inv_day_lt_5] <- as.character(as.Date(sample.dat$INVDATE[inv_day_lt_5]) - 4)
    
    sample.dat$REF_CLEAR_DATE <- as.character(sample.dat$CLEARDATE)
    sample.dat$REF_CLEAR_DATE[clear_day_lt_5] <- as.character(as.Date(sample.dat$CLEARDATE[clear_day_lt_5]) - 4)
    
    sample.dat$clear_ref_day <- get_month_day_from_date(sample.dat$REF_CLEAR_DATE)
    sample.dat$inv_ref_day   <- get_month_day_from_date(sample.dat$REF_INV_DATE)
    sample.dat$clear_ref_year <- get_year_from_date(sample.dat$REF_CLEAR_DATE)
    sample.dat$inv_ref_year   <- get_year_from_date(sample.dat$REF_INV_DATE)
    sample.dat$clear_ref_month <- get_month_from_date(sample.dat$REF_CLEAR_DATE)
    sample.dat$inv_ref_month   <- get_month_from_date(sample.dat$REF_INV_DATE)
    
    
    sample.dat.subset <- dplyr::select(sample.dat, inv_ref_year, inv_ref_month, inv_ref_day, clear_ref_year, clear_ref_month, clear_ref_day)
    # Is it the first encountered month bin ? the second or third etc.
    sample.dat$shift_index <- apply(sample.dat.subset, 1, compute_shift_index)
    sample.dat.subset <- dplyr::select(sample.dat, inv_day_label, clear_day_label, shift_index)
    sample.dat$shift_index <- apply(sample.dat.subset, 1, adjust_shift_index)
    
    sample.dat.cb <- sample.dat %>% group_by(COMPANY, ENTERPRISE, BILLTYPE) %>%
      summarise(TOTALINVOICES = n(), 
                P1_P1 = sum(inv_day_label == "P1" & clear_day_label == "P1" )/TOTALINVOICES*100,
                P1_P2 = sum(inv_day_label == "P1" & clear_day_label == "P2" )/TOTALINVOICES*100,
                P1_P3 = sum(inv_day_label == "P1" & clear_day_label == "P3" )/TOTALINVOICES*100,
                P2_P1 = sum(inv_day_label == "P2" & clear_day_label == "P1" )/TOTALINVOICES*100,
                P2_P2 = sum(inv_day_label == "P2" & clear_day_label == "P2" )/TOTALINVOICES*100,
                P2_P3 = sum(inv_day_label == "P2" & clear_day_label == "P3" )/TOTALINVOICES*100,
                P3_P1 = sum(inv_day_label == "P3" & clear_day_label == "P1" )/TOTALINVOICES*100,
                P3_P2 = sum(inv_day_label == "P3" & clear_day_label == "P2" )/TOTALINVOICES*100,
                P3_P3 = sum(inv_day_label == "P3" & clear_day_label == "P3" )/TOTALINVOICES*100,
                CLEAR_P1 = sum(clear_day_label == "P1")/TOTALINVOICES*100,
                CLEAR_P2 = sum(clear_day_label == "P2")/TOTALINVOICES*100,
                CLEAR_P3 = sum(clear_day_label == "P3")/TOTALINVOICES*100,
                DOMINANT_P1 = (CLEAR_P1 > 90),
                DOMINANT_P2 = (CLEAR_P2 > 90),
                DOMINANT_P3 = (CLEAR_P3 > 90),
                IS_DOMINANT = ((DOMINANT_P1 == T | DOMINANT_P2 == T | DOMINANT_P3 == T) & TOTALINVOICES >= 50)
      ) %>%
      ungroup()
    
    sample.dat <- left_join(sample.dat, sample.dat.cb, by=c("COMPANY", "ENTERPRISE", "BILLTYPE"))
    
    return(sample.dat)
    
  }
  
  
  
  generate_test_data <- function(date){
    
    test.data <- generate_snapshot(date)
    #test.data <- append_extra_cols_pre_test(test.data, date)
    #  test.data$true_class <- cut(test.data$DAYS_TO_PAY, breaks=breaks, labels=labels)
    
    return(test.data)
  }
  
  append_extra_cols_test <- function(test.data, result_table,last_date)
  {
    lastDate <- get_day_index(last_date)
    result_table <- result_table %>% mutate(pred_abs_days=(INV_N_DAY+pred_dtp) )
    result_table <- result_table %>% mutate(month_end_pred = (pred_abs_days <= lastDate))
    result_table <- result_table %>% mutate(true_label = ((INV_N_DAY + DAYS_TO_PAY) <= lastDate))
    
  }
  
  open_60_strip <- function(vec){
    if(vec[1]>60){
      return(FALSE)
    }else{
      return(as.logical(vec[2]))
    }
  }
  
  
  write_results <- function(result_table, resFileName){
    write.csv(result_table, paste(resultDir, "/", resFileName, sep=""), row.names=F)
  }
  
  generate_report <- function(resFile) {
    pred.data <- read.csv(paste(resultDir, '/', resFile, sep=""), header=T, stringsAsFactors=T,
                          na.strings=c("", "NA"))
    
    pred.data.good <- filter(pred.data, DAYS_OPEN <= 60)
    
    print("Prediction: Overall")
    print(table(pred.data$true_label, pred.data$month_end_pred))
    
    print("Prediction: Less than 60 days open invoices")
    print(table(pred.data.good$true_label, pred.data.good$month_end_pred))
    
  }
  
  
  
  
  train_and_test <- function(traindate, testdate, enddate, resFileName, genSnap = T) {
    print("Start : Generate train data")
    
    train.data <- generate_training_data(traindate)
    
    print("End Generating Train data")
    
    full_hist <- generate_full_histogram(train.data)
    recent_hist <- generate_recent_histogram(train.data,traindate)
    custno_hist <- generate_custno_histogram(train.data)
    
    
    print("Created all histograms")
    
    if (genSnap == T) {
      print("Creating snapshot")
      test.data <- generate_test_data(testdate)
    } else {
      print("Using existing snapshot")
      test.data <- read.csv(paste(resultDir, '/', resFileName, sep=""), header=T,
                            stringsAsFactors=T, na.strings=c("", "NA"))
    }
    
    
    print("Generated test Snapshot")
    result_table <- predict_splitClassifier(custno_hist,recent_hist,full_hist, test.data)
    print("predict_countClassifier completed successfully")
    result_table <- append_extra_cols_test(test.data,result_table,enddate)
    print("Append extra cols test")
    write_results(result_table, resFileName)
    print("End Writing the results")
    
    return(result_table)
  }
  
  
  # Generate performance report for top companies model with backtesting:
  
  plot_histogram <- function(histvec) {
    
    df.hist <- data.frame(x = 5*1:length(histvec), y = histvec)
    return (ggplot(df.hist, aes(x=x, y = y)) + geom_bar(stat="identity", fill="blue"))
  }
  
  generate_top_company_report <- function(runDate, endDate, monthLabel) {
    
    #runDate <- '2016-01-07'
    #endDate <- '2016-01-31'
    #monthLabel <- 'JAN'
    
    entr_calc_data.dat <- generate_training_data(runDate)
    full_histogram <- generate_full_histogram(entr_calc_data.dat)
    
    histCounts <- apply(full_histogram[, grep("FULL_C", names(full_histogram))], 1, sum)
    full_histogram$TOTALCOUNT <- histCounts
    
    # Look at biggest company, enterprise, billtypes
    # 20% of top groups account for 80% of the data
    full_histogram.desc <- arrange(full_histogram, desc(TOTALCOUNT))
    full_histogram.desc$CUMCOUNT <- cumsum(full_histogram.desc$TOTALCOUNT)
    #maxCumCount <- max(full_histogram.desc$CUMCOUNT)
    #top.10000.dat <- full_histogram.desc %>% filter(CUMCOUNT <= 0.7 * maxCumCount)
    
    top.10000.dat <- full_histogram.desc[1:5000, ]
    
    top.10000.dat$ENTROPY_CAT <- cut(top.10000.dat$FULL_entropy, 
                                     breaks=c(-Inf, 0.25, 0.75, 1.5, 2.0, 3.0, Inf), 
                                     labels=c("VERY LOW", "LOW", "MEDIUM", "MEDIUM HIGH", "HIGH", "VERY HIGH"))
    
    top.10000.good   <- top.10000.dat %>% filter(ENTROPY_CAT == "VERY LOW" | ENTROPY_CAT == "LOW")
    top.10000.medium <- top.10000.dat %>% filter(ENTROPY_CAT == "MEDIUM" | ENTROPY_CAT == "MEDIUM HIGH")
    top.10000.high   <- top.10000.dat %>% filter(ENTROPY_CAT == "HIGH")
    
    # report accuracy of the model
    train.data <- generate_training_data(runDate)
    full_hist <- generate_full_histogram(train.data)
    recent_hist <- generate_recent_histogram(train.data, runDate)
    custno_hist <- generate_custno_histogram(train.data)
    print("Created all histograms")
    
    test.data <- generate_test_data(runDate)
    
    
    result_table <- predict_splitClassifier(custno_hist,recent_hist,full_hist, test.data)
    res1.all <- append_extra_cols_test(test.data,result_table,endDate)
    
    # Build Accuracy Table
    res1.all.good <- left_join(res1.all, top.10000.good, c("COMPANY", "BILLTYPE", "ENTERPRISE"))
    
    res1.all.good <- res1.all.good[complete.cases(res1.all.good), ]
    
    res1.all.medium <- left_join(res1.all, top.10000.medium, c("COMPANY", "BILLTYPE", "ENTERPRISE"))
    
    res1.all.medium <- res1.all.medium[complete.cases(res1.all.medium), ]
    
    res1.all.high <- left_join(res1.all, top.10000.high, c("COMPANY", "BILLTYPE", "ENTERPRISE"))
    
    res1.all.high <- res1.all.high[complete.cases(res1.all.high), ]
    
    
    res1.medium.60 <- filter(res1.all.medium, DAYS_OPEN <= 60)
    res1.good.60 <- filter(res1.all.good, DAYS_OPEN <= 60)
    res1.high.60 <- filter(res1.all.high, DAYS_OPEN <= 60)
    
    # Generate Table
    colVol1 <- c(nrow(res1.all.good), nrow(res1.all.medium), nrow(res1.all.high))
    colAcc1 <- c(nrow(filter(res1.all.good, true_label == month_end_pred)),
                 nrow(filter(res1.all.medium, true_label == month_end_pred)),
                 nrow(filter(res1.all.high, true_label == month_end_pred)))
    
    colVol2 <- c(nrow(res1.all.good %>% filter(DAYS_OPEN <= 60)),
                 nrow(res1.all.medium %>% filter(DAYS_OPEN <= 60)),
                 nrow(res1.all.high %>% filter(DAYS_OPEN <= 60)))
    
    colAcc2 <- c(nrow(res1.all.good %>% filter(DAYS_OPEN <= 60 & true_label == month_end_pred)),
                 nrow(res1.all.medium %>% filter(DAYS_OPEN <= 60 & true_label == month_end_pred)),
                 nrow(res1.all.high %>% filter(DAYS_OPEN <= 60 & true_label == month_end_pred)))
    
    
    col1 <- c("LOW ( < 0.75 )", "MEDIUM (0.75 - 2.25)", "HIGH (> 2.25)")
    
    
    summ.dat <- data.frame(ENTROPY = col1,
                           OVERALL_VOLUME = colVol1,
                           OVERALL_ACC = colAcc1/colVol1 * 100,
                           UNDER_60_VOLUME = colVol2,
                           UNDER_60_ACC = colAcc2/colVol2 * 100,
                           TOTAL_TEST_INV = nrow(res1.all),
                           MON = monthLabel)
    
    
    return(summ.dat)
  }
  
  generate_top_company_report_ma <- function(runDate, endDate, monthLabel) {
    
    #  runDate <- '2016-01-07'
    #  endDate <- '2016-01-31'
    #  monthLabel <- 'JAN'
    
    entr_calc_data.dat <- generate_training_data(runDate)
    full_histogram <- generate_month_aligned_histogram(entr_calc_data.dat)
    
    full_histogram <- sample.ma.hist
    #histCounts <- apply(full_histogram[, grep("FULL_C", names(full_histogram))], 1, sum)
    full_histogram$TOTALCOUNT <- full_histogram$histCounts
    
    # Look at biggest company, enterprise, billtypes
    # 20% of top groups account for 80% of the data
    full_histogram.desc <- arrange(full_histogram, desc(TOTALCOUNT))
    full_histogram.desc$CUMCOUNT <- cumsum(full_histogram.desc$TOTALCOUNT)
    #maxCumCount <- max(full_histogram.desc$CUMCOUNT)
    #top.10000.dat <- full_histogram.desc %>% filter(CUMCOUNT <= 0.7 * maxCumCount)
    
    top.10000.dat <- full_histogram.desc[1:5000, ]
    
    top.10000.dat$ENTROPY_CAT <- cut(top.10000.dat$MONTH_ALIGNED_ENTROPY, 
                                     breaks=c(-Inf, 0.25, 1.0, 1.5, 2.0, 3.0, Inf), 
                                     labels=c("VERY LOW", "LOW", "MEDIUM", "MEDIUM HIGH", "HIGH", "VERY HIGH"))
    
    top.10000.good   <- top.10000.dat %>% filter(ENTROPY_CAT == "VERY LOW" | ENTROPY_CAT == "LOW")
    top.10000.medium <- top.10000.dat %>% filter(ENTROPY_CAT == "MEDIUM" | ENTROPY_CAT == "MEDIUM HIGH")
    top.10000.high   <- top.10000.dat %>% filter(ENTROPY_CAT == "HIGH")
    
    # report accuracy of the model
    #train.data <- generate_training_data(runDate)
    #full_hist <- generate_full_histogram(train.data)
    #recent_hist <- generate_recent_histogram(train.data, runDate)
    #custno_hist <- generate_custno_histogram(train.data)
    #print("Created all histograms")
    
    test.data <- generate_test_data(runDate)
    
    
    result_table <- predict_splitClassifier_ma(full_histogram, test.data)
    res1.all <- append_extra_cols_test(test.data,result_table,endDate)
    
    # Build Accuracy Table
    res1.all.good <- left_join(res1.all, top.10000.good, c("COMPANY", "BILLTYPE", "ENTERPRISE"))
    
    res1.all.good <- res1.all.good[complete.cases(res1.all.good), ]
    
    res1.all.medium <- left_join(res1.all, top.10000.medium, c("COMPANY", "BILLTYPE", "ENTERPRISE"))
    
    res1.all.medium <- res1.all.medium[complete.cases(res1.all.medium), ]
    
    res1.all.high <- left_join(res1.all, top.10000.high, c("COMPANY", "BILLTYPE", "ENTERPRISE"))
    
    res1.all.high <- res1.all.high[complete.cases(res1.all.high), ]
    
    
    res1.medium.60 <- filter(res1.all.medium, DAYS_OPEN <= 60)
    res1.good.60 <- filter(res1.all.good, DAYS_OPEN <= 60)
    res1.high.60 <- filter(res1.all.high, DAYS_OPEN <= 60)
    
    # Generate Table
    colVol1 <- c(nrow(res1.all.good), nrow(res1.all.medium), nrow(res1.all.high))
    colAcc1 <- c(nrow(filter(res1.all.good, true_label == month_end_pred)),
                 nrow(filter(res1.all.medium, true_label == month_end_pred)),
                 nrow(filter(res1.all.high, true_label == month_end_pred)))
    
    colVol2 <- c(nrow(res1.all.good %>% filter(DAYS_OPEN <= 60)),
                 nrow(res1.all.medium %>% filter(DAYS_OPEN <= 60)),
                 nrow(res1.all.high %>% filter(DAYS_OPEN <= 60)))
    
    colAcc2 <- c(nrow(res1.all.good %>% filter(DAYS_OPEN <= 60 & true_label == month_end_pred)),
                 nrow(res1.all.medium %>% filter(DAYS_OPEN <= 60 & true_label == month_end_pred)),
                 nrow(res1.all.high %>% filter(DAYS_OPEN <= 60 & true_label == month_end_pred)))
    
    
    col1 <- c("LOW ( < 0.75 )", "MEDIUM (0.75 - 2.25)", "HIGH (> 2.25)")
    
    
    summ.dat <- data.frame(ENTROPY = col1,
                           OVERALL_VOLUME = colVol1,
                           OVERALL_ACC = colAcc1/colVol1 * 100,
                           UNDER_60_VOLUME = colVol2,
                           UNDER_60_ACC = colAcc2/colVol2 * 100,
                           TOTAL_TEST_INV = nrow(res1.all),
                           MON = monthLabel)
    
    
    return(summ.dat)
  }
  
  
  
  
  # Sample histograms:
  # Try to see month-end-aligned clearing
 

main_func <- function(runDate, endDate, PaidInv){
 print("Initializing corpus")
 progress$set(message="Initializing corpus", value = 2)
  initialize_corpus_from_cache(PaidInv)
  print(names(paidinvoices.corpus))
  print(runDate)
  print(endDate)
  runDate1 <- '2016-01-07'
  runDate2 <- '2016-02-07'
  runDate3 <- '2016-03-07'
  runDate4 <- '2016-04-07'
  runDate5 <- '2016-05-07'
  
  endDate1 <- '2016-01-31'
  endDate2 <- '2016-03-01'
  endDate3 <- '2016-03-31'
  endDate4 <- '2016-04-30'
  endDate5 <- '2016-05-31'
  
  #runDate <- runDate2
  #endDate <- endDate2
  progress <- shiny::Progress$new(min=1, max = 6)
  
 progress$set(message="Generating Training data", value = 3)
  #progress$inc(1/n, detail = "Generating Training data")
  sample.dat.1 <- generate_training_data(runDate)
  #sample.ma.hist.1 <- generate_month_aligned_histogram(sample.dat.1)
 
 progress$set(message="Generating Histogram", value = 4)
  #progress$inc(1/n, detail = "Generating Histogram")
  sample.full.hist.1 <- generate_full_histogram(sample.dat.1)
  
 progress$set(message="Generating Test Data", value = 5)
  #progress$inc(1/n, detail = "Generating Test Data")
  test.data <- generate_test_data(runDate)
  
  print("Done generation of test data")
 
 progress$set(message="Making Predictions", value = 6)
  #progress$inc(1/n, detail = "Making Predictions")
  result_table <- predict_splitClassifier(sample.full.hist.1,sample.full.hist.1,sample.full.hist.1, test.data)
  print("Done predicting")
 
 progress$set(message="Appending extra columns to the predictions", value = 7)
  #progress$inc(1/n, detail = "Appending extra columns to the predictions")
  result.full.1 <- append_extra_cols_test(test.data, result_table,endDate)
  print("Done adding extra cols to test data")
 progress$set(message="Completed", value = 8)
 #progress$close()
  
  total <- nrow(result.full.1)
  tab.full <- table(result.full.1$true_label, result.full.1$month_end_pred)
  print("got the confusion table")
  return(result.full.1)
}
  
  #tab.full <- table(result.full.1$true_label, result.full.1$month_end_pred)
  #acc.full <- nrow(filter(result.full.1, true_label == month_end_pred))/total
  
  
  #Get all details per CEB
  # dat1 <- result.full.1 %>% group_by(COMPANY, ENTERPRISE, BILLTYPE) %>%
  #                      summarise(TOTAL_INVOICES = n(), TOTAL_TRUE = sum(true_label==T),
  #                       TOTAL_FALSE=sum(true_label == F),
  #                       PREDICTED_CORRECT = sum(true_label==month_end_pred),
  #                       PRED_TT =sum(true_label == T & month_end_pred == T), 
  #                       PRED_TF =sum(true_label == T & month_end_pred == F), 
  #                       PRED_FT =sum(true_label == F & month_end_pred == T), 
  #                       PRED_FF =sum(true_label == F & month_end_pred == F)
  #                       #GROSSINV_CB = sum(GROSSINV),
  #                       #AMT_TT = sum((true_label == T & month_end_pred == T)*GROSSINV),
  #                       #AMT_TF = sum((true_label == T & month_end_pred == F)*GROSSINV),
  #                       #AMT_FT = sum((true_label == F & month_end_pred == T)*GROSSINV),
  #                       #AMT_FF = sum((true_label == F & month_end_pred == F)*GROSSINV),
  #                       #AMT_TT_FF = abs(AMT_TT) + abs(AMT_FF) + 1,
  #                       #AMT_TF_FT = abs(AMT_TF) + abs(AMT_FT)  + 1 ,
  #                       #GOODNESS_RATIO = (AMT_TT_FF/AMT_TF_FT),
  #                       #DOLLAR_ACC = ((AMT_TT_FF*100/(AMT_TF_FT+AMT_TT_FF)))
  #                       
  # #                       AMT_TT_N = sum((true_label == T & month_end_pred == T)*GROSSINV)/AMT_TT,
  # #                       AMT_TF_N = sum((true_label == T & month_end_pred == F)*GROSSINV)/AMT_TT,
  # #                       AMT_FT_N = sum((true_label == F & month_end_pred == T)*GROSSINV)/AMT_TT,
  # #                       AMT_FF_N = sum((true_label == F & month_end_pred == F)*GROSSINV)/AMT_TT
  #                       ) %>% ungroup()
  # dat1 <- arrange(dat1, desc(TOTAL_INVOICES))
  #dat1 <- arrange(dat1, desc(GROSSINV_CB))
  
  # sum(abs(dat1$AMT_TT) + abs(dat1$AMT_FF)) / 
  #           (sum(abs(dat1$AMT_TT) + abs(dat1$AMT_FF))
  #             + sum(abs(dat1$AMT_TF) + abs(dat1$AMT_FT)) )*100
  
  
  # res.jan <- dplyr::select(result.full.1, CUSTNO, INVNO, 
  #                TIMESTAMP, COMPANY, ENTERPRISE, BILLTYPE,
  #                pred_dtp, month_end_pred)
  # 
  # write.csv(res.jan, "Results/Pred_FullHist_07Jan2016.csv", row.names = F)
  
  # res.feb <- dplyr::select(result.full.1, CUSTNO, INVNO, 
  #                TIMESTAMP, COMPANY, ENTERPRISE, BILLTYPE,
  #                pred_dtp, month_end_pred)
  # 
  # write.csv(res.feb, "Results/Pred_FullHist_07Feb2016.csv", row.names = F)
  
  # res.mar <- dplyr::select(result.full.1, CUSTNO, INVNO, 
  #                TIMESTAMP, COMPANY, ENTERPRISE, BILLTYPE,
  #                pred_dtp, month_end_pred)
  # 
  # write.csv(res.mar, "Results/Pred_FullHist_07Mar2016.csv", row.names = F)
  
  # res.apr <- dplyr::select(result.full.1, CUSTNO, INVNO, 
  #                TIMESTAMP, COMPANY, ENTERPRISE, BILLTYPE,
  #                pred_dtp, month_end_pred)
  # 
  # write.csv(res.apr, "Results/Pred_FullHist_07Apr2016.csv", row.names = F)
  
  # res.may <- dplyr::select(result.full.1, CUSTNO, INVNO, 
  #                TIMESTAMP, COMPANY, ENTERPRISE, BILLTYPE,
  #                pred_dtp, month_end_pred)
  # 
  # write.csv(res.may, "Results/Pred_FullHist_07May2016.csv", row.names = F)
  
  
  
  # res.may <- dplyr::select(result.full.1, CUSTNO, INVNO, 
  #                TIMESTAMP, COMPANY, ENTERPRISE, BILLTYPE,
  #                DAYS_TO_PAY ,pred_dtp, month_end_pred, true_label)
  
  #sample.hist <- generate_full_histogram(sample.dat)
  #Get the 1% companies where there is strong dominence for month aligned prediction
  
  
  
  
  ################################# FULL HIST FILE END   ######################################
  
  PaidInv <- reactive({
    
    # input$file1 will be NULL initially. After the user selects
    # and uploads a file, it will be a data frame with 'name',
    # 'size', 'type', and 'datapath' columns. The 'datapath'
    # column will contain the local filenames where the data can
    # be found.

    TrainFile <- input$train_file

    if (is.null(TrainFile))
      return(NULL)
    progress$set(message="Reading the csv file", value = 1)
    train.df <- read.csv(TrainFile$datapath, header=T)
   
    return (train.df)
      #df.small <- filter(df, BILLTYPE== "IGF")
    #return (df.small)
  })
  
  func <- function(df){
    t <- df %>% group_by(COMPANY, ENTERPRISE, BILLTYPE) %>% summarise(g_cnt = n())
    t.desc <- arrange(t, desc(g_cnt))
    return(t.desc)
  }
output$message <- renderText({
    resultPredictions()
    paste("Done, you may download the predictions for", input$runDate)
})
  resultPredictions <- eventReactive(input$train_button,{
    print("Getting PaidInv)")
    paidinv <- PaidInv()
    paidinv <- data.frame(paidinv)
    print(nrow(paidinv))
    #openinv <- OpenInv()
    r <- main_func(input$runDate, input$endDate, paidinv)
    #r <- func(paidinv)
    #r <- data.frame(input$EndDate)
    return (r)
    #filter(rt, BILLTYPE == "IGF")
  })
output$downloadPred <- downloadHandler(
  filename = function() { 
    paste(input$runDate, '_pred.csv', sep='') 
  },
  content = function(file) {
    write.csv(resultPredictions(), file)
  }
)
 # ntext <- eventReactive(input$goButton, {
#    input$n
 # })
  
  #output$nText <- renderText({
  #  ntext()
  #})
})
