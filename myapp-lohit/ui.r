library(shiny)

shinyUI(fluidPage(
  titlePanel("Uploading Files"),
  sidebarLayout(
    sidebarPanel(
      fileInput('train_file', 'Choose PaidInv data (CSV File)',
                accept=c('text/csv', 
								 'text/comma-separated-values,text/plain', 
								 '.csv','application/x-compressed')),
      actionButton("train_button", "Train and test!"),
      selectInput("runDate", label = h3("Select RunDate"), 
                  choices = list("Jan 07" = "2016-01-07","Feb 07" = "2016-02-07","Mar 07" = "2016-03-07", 
                  selected = "2016-01-07")),
      
                  selectInput("endDate", label = h3("Select EndDate"), 
                              choices = list("Jan 31" = "2016-01-31","Feb 28" = "2016-02-28","Mar 31" = "2016-03-31", 
                                             selected = "2016-01-31")),
      downloadButton('downloadPred', 'Download predictions')
      
# selectInput("select", label = h3("Select box"), 
#             choices = list("Choice 1" = 1, "Choice 2" = 2, "Choice 3" = 3), 
#             selected = 1)

#       selectInput("RunDate", label = h3("Select RunDate"), 
#                   choices = list("Jan 07" = 1,"Feb 07" = 2,"Mar 07" = 3, 
#                                  selected = 1)),
#       
#       selectInput("EndDate", label = h3("Select EndDate"), 
#                   choices = list("Jan 31" = 1,"Feb 28" = 2,"Mar 31" = 3, 
#                                  selected = 1))
      #fileInput('test_file', 'Choose Test data (CSV File)',
      #          accept=c('text/csv', 
      #                   'text/comma-separated-values,text/plain', 
      #                   '.csv')),
      #actionButton("test_button", "Test!")

    ),
    mainPanel(
      textOutput('message')
      
    )
  )
))
