# DOCKER-VERSION 1.4.1

####################################################################
###                                                              ###
###   Based on original Dockerfile from                          ###
###   https://github.com/nickstenning/dockerfiles/blob/master/   ###
###   etherpad-lite/Dockerfile                                   ###
###                                                              ###
####################################################################

from registry.ng.bluemix.net/ibmnode:latest
MAINTAINER jtmoore@us.ibm.com

# Install other required packages
run apt-get -y update
run apt-get -y install curl git-core python libssl-dev pkg-config build-essential software-properties-common python-software-properties r-base r-base-dev
run apt-get clean

# Install shiny
run rm -rf /src
run mkdir /src
#run cd /src
run sudo git clone https://lohitkrishnan:gointosleep@bitbucket.org/lohitkrishnan/invoices-lohit.git /src/invoices-lohit
#run ls /src/
#run sudo git clone https://hub.jazz.net/git/jtmoore/shiny-r-starter /src/shiny-r-starter
run chmod +x /src/invoices-lohit/scripts/install_shiny.r
#run echo 'library(shiny)' > /src/shiny-r-starter/scripts/start_server_shiny.r
#run echo 'runExample("01_hello",port = 9080, host="0.0.0.0")' >> /src/shiny-r-starter/scripts/start_server_shiny.r

run ls /src/
add ./settings.json /src/invoices-lohit/settings.json
run ./src/invoices-lohit/scripts/install_shiny.r

expose  :9080

entrypoint  ["/src/invoices-lohit/scripts/start.sh", "--root"]

