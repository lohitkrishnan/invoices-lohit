#!/bin/bash

#stop all docker containers
ice --local stop $(sudo ice --local ps -a -q)

#remove all stoped docker containers
ice --local rm $(sudo ice --local ps -a -q)

#build the new container
ice --local build --no-cache -t shiny-starter .

#run the container
ice --local  run -it -d -p 9080:9080 shiny-starter

#list the docker containers
ice --local ps
