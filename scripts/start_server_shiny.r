#!/usr/bin/Rscript

#loads shiny library
library(shiny)

#runs an example app on port 9080
#runExample("01_hello",port = 9080,host="0.0.0.0")

#Runs an app with a server.R and ui.R in the folder
runApp("/src/invoices-lohit/myapp-lohit",port = 9080,host="0.0.0.0")
