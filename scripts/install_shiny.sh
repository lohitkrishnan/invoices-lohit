#!/bin/bash

#download shiny install package
su - \
-c "R -e \"install.packages('shiny', repos='http://cran.rstudio.com/')\""

#install installer for shiny packages
apt-get install -y gdebi-core

#get shiny server packages
wget http://download3.rstudio.org/ubuntu-12.04/x86_64/shiny-server-1.3.0.403-amd64.deb

#install shiny server
gdebi --non-interactive shiny-server-1.3.0.403-amd64.deb
